import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from "typeorm";
import { User } from '../users/user.entity';
import { ItemList } from "./item-list.entity";

@Entity()
export class ShoppingList{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    purchasingDate: string;

    @ManyToOne(() => User, (user) => user.shoppingLists, { cascade: true })
    user: User;

    @OneToMany(() => ItemList, (itemLists) => itemLists.shoppingList)
    itemLists: ItemList[];

}