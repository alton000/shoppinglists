import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { ShoppingList } from "./shopping-list.entity";

@Entity()
export class ItemList{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    productName: string;

    @Column()
    quantity: number;

    @ManyToOne(() => ShoppingList, (shoppingList) => shoppingList.itemLists, { cascade: true })
    shoppingList: ShoppingList;
}