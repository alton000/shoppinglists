import { Controller } from '@nestjs/common';
import { Post, Get, Patch, Param, Query, Body } from '@nestjs/common';
import { CreateShoppingListDto } from './dtos/create-shoppingList.dto';
import { UsersService } from '../users/users.service';
import { ShoppingListService } from './shopping-list.service';
import { CreateItemListDto } from './dtos/create-itemlist.dto';

@Controller('shopping-list')
export class ShoppingListController {
    constructor(private usersService: UsersService, 
        private shoppingListService: ShoppingListService){}

    @Post()
    async create(@Body() body: CreateShoppingListDto){
        const user = await this.usersService.findOne(body.userId);

        return this.shoppingListService.create(body.purchasingDate, user);
    }

    @Post('/push')
    pushItem(@Body() body: CreateItemListDto){
        return this.shoppingListService.pushItem(body.shoppingListId,body.productName,body.quantity);
    }

    @Get('/:id')
    getAllItems(@Param('id') id: string){
        return this.shoppingListService.getAllItemLists(parseInt(id));
    }

    @Post('/pushMass')
    pushItemsInMass(@Body() body: CreateItemListDto[]){
        body.forEach( (element) => {
            this.pushItem(element);
        });
    }

    @Get('/date/:date')
    test(@Param('date') date: string){
        return this.shoppingListService.joinShoppingListsInOneDate(date);
    }
}
