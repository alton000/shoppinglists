import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ShoppingList } from './shopping-list.entity';
import { ItemList } from './item-list.entity';
import { User } from '../users/user.entity';
import { ProductsService } from '../products/products.service';


@Injectable()
export class ShoppingListService {
    constructor(
        @InjectRepository(ShoppingList) private shopRepo: Repository<ShoppingList>,
        @InjectRepository(ItemList) private itemListRepo: Repository<ItemList>,
        private productsService: ProductsService){}

    create(date: string, user: User){
        const shoppingList = this.shopRepo.create({ purchasingDate : date, user: user, itemLists: [] });

        return this.shopRepo.save(shoppingList);
    }

    findOne(id: number){
        return this.shopRepo.findOne({ where: { id: id }, relations: { itemLists: true }});
    }

    async remove(id: number){
        const shoppingList = await this.findOne(id);

        if(!shoppingList){
            throw new NotFoundException('Shopping List Not Found');
        }

        return this.shopRepo.remove(shoppingList);
    }

    find(purchasingDate: string){
        return this.shopRepo.find({ where: { purchasingDate: purchasingDate }, relations: { itemLists: true } });
    }

    update(id: number, attrs: Partial<ShoppingList>){
        const shoppingList = this.findOne(id);

        if(!shoppingList){
            throw new NotFoundException('item not found');
        }

        Object.assign(shoppingList, attrs);
    }

    async pushItem(shoppingListId: number, productName: string, quantity: number){
        if(quantity<=0){
            throw new Error('Invalid Quantity');
        }

        const product = await this.productsService.findProduct(productName);

        if(!product){
            throw new BadRequestException('Product Name not Valid');
        }

        const shoppingList = await this.findOne(shoppingListId);

        if(!shoppingList){
            throw new NotFoundException('Shopping list not found!')
        }

        const itemList = this.itemListRepo.create({productName: productName, quantity: quantity, shoppingList: shoppingList});

        return this.itemListRepo.save(itemList);
    }

    async getAllItemLists(shoppingListId: number){
        const shoppingList = await this.findOne(shoppingListId);

        return shoppingList.itemLists;
    }

    async getShoppingListIdsArray(date: string){
        const shoppingListIds = await this.shopRepo.query(`select id from shopping_list where purchasingDate="${date}"`);
        
        let array: string[] = [];

        shoppingListIds.forEach(element => {
            array.push(element['id'])
        });

        return array;
    }

    async getWhereConditionsForItemLists(date: string){
        const array = await this.getShoppingListIdsArray(date);
        let conditionsWhere: string = "";
        array.forEach(element => {
            conditionsWhere += "shoppingListId="+element+" or ";
        });

        conditionsWhere = conditionsWhere.substring(0,conditionsWhere.length-4)

        return conditionsWhere;
    }

    async joinShoppingListsInOneDate(date: string){
        const conditionsWhere = await this.getWhereConditionsForItemLists(date);
        return await this.itemListRepo.query(`select productName, sum(quantity) from item_list where ${conditionsWhere} group by productName`);
    }


}
