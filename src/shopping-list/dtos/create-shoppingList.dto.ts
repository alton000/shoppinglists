import { IsNumber, IsString, IsDateString } from "class-validator";

export class CreateShoppingListDto{
    
    @IsDateString()
    purchasingDate: string;

    @IsNumber()
    userId: number;
}