import { IsNumber, IsString } from "class-validator";

export class CreateItemListDto{
    @IsString()
    productName: string;

    @IsNumber()
    quantity: number;
    
    @IsNumber()
    shoppingListId: number;
}