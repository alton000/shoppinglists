import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { ProductsModule } from './products/products.module';
import { ShoppingListModule } from './shopping-list/shopping-list.module';
import { User } from './users/user.entity';
import { Products } from './products/products.entity';
import { ShoppingList } from './shopping-list/shopping-list.entity';
import { ItemList } from './shopping-list/item-list.entity';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'sqlite',
    database: 'db.sqlite',
    entities:[User, Products, ShoppingList, ItemList],
    synchronize: true,
  }),
    UsersModule, ProductsModule, ShoppingListModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
