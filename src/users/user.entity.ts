import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { ShoppingList } from "../shopping-list/shopping-list.entity";

@Entity()
export class User{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    username: string;

    @Column()
    password: string;

    @OneToMany(() => ShoppingList, (shoppingLists) => shoppingLists.user)
    shoppingLists: ShoppingList[];
}