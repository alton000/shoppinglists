import { Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';


@Injectable()
export class UsersService {
    constructor(@InjectRepository(User) private repo: Repository<User>,){}

    create(username: string, password: string){
        const user = this.repo.create({ username, password });

        return this.repo.save(user);
    }

    findOne(id: number) {
        return this.repo.findOne( {where: { id: id }, relations: { shoppingLists: true }} );
    }

    find(name: string){
        return this.repo.find( { where: { username: name }, relations: { shoppingLists: true } } );
    }

    async remove(id: number){
        const user = await this.findOne(id);

        if(!user){
            throw new NotFoundException('user not found');
        }

        return this.repo.remove(user);
    }

    async update(id: number, attrs: Partial<User>){
        const user = await this.findOne(id);

        if(!user){
            throw new NotFoundException('user not found');
        }

        Object.assign(user, attrs);

        return this.repo.save(user);
    }
}
