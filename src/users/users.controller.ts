import { Controller } from '@nestjs/common';
import { Post, Get, Param, Body, Patch } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dtos/create-user.dto';
import { UpdateUserDto } from './dtos/update-user.dto';

@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService){}
    @Post('/signup')
    createUser(@Body() body: CreateUserDto){
        return this.usersService.create(body.username, body.password);
    }

    @Get('/:id')
    findUser(@Param('id') id: string){

        return this.usersService.findOne(parseInt(id));
    }

    @Get('/find/:username')
    find(@Param('username') username: string){
        return this.usersService.find(username);
    }

    @Get('/remove/:id')
    remove(@Param('id') id: string){
        return this.usersService.remove(parseInt(id));
    }

    @Patch('/:id')
    updateUser(@Param('id') id: string, @Body() body: UpdateUserDto){
        return this.usersService.update(parseInt(id), body);
    }
}
