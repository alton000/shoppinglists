import { IsString, IsOptional } from "class-validator";

export class UpdateUserDto{
    @IsOptional()
    @IsString()
    username: string;

    @IsString()
    @IsOptional()
    password: string;
}