import { Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Products } from './products.entity';


@Injectable()
export class ProductsService {
    constructor(@InjectRepository(Products) private prodRepo: Repository<Products>){}

    getAll(){
        return this.prodRepo.find();
    }

    create(name: string){
        const product = this.prodRepo.create( { productName: name, active: true } );

        return this.prodRepo.save(product);
    }

    findProduct(name: string){
        return this.prodRepo.findOne( { where: { productName: name } } );
    }

    findOne(id: number){
        return this.prodRepo.findOne( { where: { id: id } } );
    }

    async updateProduct(id: number, attrs: Partial<Products>){
        const product = await this.findOne(id);

        if(!product){
            throw new NotFoundException('Product not Found');
        }

        
        Object.assign(product, attrs);

        return this.prodRepo.save(product);
    }

    activateProduct(id: number){
        return this.updateProduct(id, { active: true } );
    }

    deactivateProduct(id: number){
        return this.updateProduct(id, { active: false } );
    }

}
