import { IsString } from "class-validator";

export class CreateProductsDto{
    @IsString()
    name: string;
}