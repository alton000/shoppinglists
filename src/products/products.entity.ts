import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Products{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    productName: string;

    @Column()
    active: boolean;
}