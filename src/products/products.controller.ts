import { Controller } from '@nestjs/common';
import { Post, Patch, Get, Body, Param } from '@nestjs/common';
import { CreateProductsDto } from './dtos/create-products.dto';
import { ProductsService } from './products.service';

@Controller('products')
export class ProductsController {
    constructor(private productsService: ProductsService){}
    @Post()
    create(@Body() body: CreateProductsDto){
        return this.productsService.create(body.name);
    }

    @Get('/all')
    findAll(){
        return this.productsService.getAll();
    }

    @Get('/:name')
    findProductByName(@Param('name') name: string){
        return this.productsService.findProduct(name);
    }

    @Post('/activate/:id')
    activateProduct(@Param('id') id: string){
        return this.productsService.activateProduct(parseInt(id));
    }

    @Post('/deactivate/:id')
    deactivateProduct(@Param('id') id: string){
        return this.productsService.deactivateProduct(parseInt(id));
    }

}
