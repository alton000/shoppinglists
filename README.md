# Shopping Lists Project

This is a very simple NestJs portfolio project that essentially makes shopping Lists for a group of stores so the headquarters can join all of them and make the monthly purchase.

## The Project's SQL structure
![Structure for SQL](/images/SQLstructure.png)

## Why I chose SQL

I chose SQL because the project had very simple need and I knew there will be no need for flexibility in the data, so SQL is easier to troubleshoot and makes life easier then a more nimble NoSql structure.